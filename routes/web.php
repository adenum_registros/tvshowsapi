<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(['prefix' => 'tvshow/apiv1/'], function ($app) {
    $app->get('/','TVShowController@index'); //get all the routes
    $app->get('/{search}','TVShowController@show'); //get all the routes
});