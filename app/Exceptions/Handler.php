<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {           
        // if http request not found
        if ($e instanceof NotFoundHttpException) {  
            $tvshowApiv1Uri = config('constants.APP_URI');
            $uri = $request->getUri();
            $uri = rtrim($uri, '/') . '/';
            // Url API valid, but not string to find
            // (../tvshow/apiv1/ or ../tvshow/apiv1)
            if(strpos($uri, $tvshowApiv1Uri) !== false){
                $response = array(
                    "name"=>"Bad Request",
                    "message"=>"Missing required string: tv show title",
                    "status"=>200);
            } else {
                // Url not valid
                // (ie ../tvshow/apiiiiiv1)
                $response = array(
                    "name"=>"Not found",
                    "message"=>"Page not found",
                    "status"=>404);
            }
            
        } else if($e instanceof \GuzzleHttp\Exception\ClientException){
            // third party API not found
            $response = array(
                "name"=>"Service temporarily unavailable",
                "message"=>"Please try again later",
                "status"=>503);
        }   
        
        if(!empty($response)){
            return response()->json($response, $response['status']);
        }
                
        return parent::render($request, $e);
    }
}
