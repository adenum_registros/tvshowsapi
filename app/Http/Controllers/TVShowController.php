<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class TVShowController extends Controller {

    /**
     * Return JSON with the tv shows founded. 
     * The search can be the entire title or a substring.
     * The results for each search are cached, to minimize third party api calls.
     * @param string $search
     * @return JSON
     */
    public function show($search) {
        $tvshows = array();
        $searchLower = strtolower($search);

        // use Redis cache to get the data if was previously saved
        $tvshowTemp = Redis::get('tvshow:query:' . $searchLower);
        if ($tvshowTemp) {
            $tvshows = unserialize($tvshowTemp);
            // quick test to show if Redis works (and response was cached)
            //$tvshows['cached'] = 1;
        }

        // not in cache - try by third API
        if (empty($tvshows)) {
            $timeout = 8;
            $baseUriThirdApi = 'http://api.tvmaze.com/search/shows';
            $searchParam = '?q=' . $search;
            $client = new Client(['base_uri' => $baseUriThirdApi]);
            // Send a request 
            $res = $client->request('GET', $searchParam, ['connect_timeout' => $timeout]);

            $tvshowsListThirdApi = json_decode($res->getBody(), true);
            foreach ($tvshowsListThirdApi as $tvshowTmp) {
                $tvshowTmpTitleLower = strtolower($tvshowTmp['show']['name']);
                // non-case sensitive and non-typo tolerant
                // The search can be a substring
                if (strpos($tvshowTmpTitleLower, $searchLower) !==false){
                    $tvshows[] = [
                        'score' =>$tvshowTmp['score'],
                        'title' => $tvshowTmp['show']['name']
                    ];
                }
            }
        }

        // finally found results
        if (!empty($tvshows)) {
            $response = $tvshows;
            Redis::set('tvshow:query:' . $searchLower, serialize($tvshows));
        } else {
            // title (or substring) not found
            $response = array(
                "name" => "TVShow not found",
                "message" => "Try again",
                "status" => 200);
        }

        return response()->json($response);
    }

}
