<?php

class TVShowTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testQuery()
    {
        $this->json('GET', '/tvshow/apiv1/'.'Darkness')
             ->seeJson([
               'title' => 'Darkness',
        ]);
        
        $this->json('GET', '/tvshow/apiv1/'.'deadpool')
             ->seeJson([
               'title' => 'Deadpool',
        ]);        
        
        $this->json('GET', '/tvshow/apiv1/'.'asdasd')
             ->seeJson([
               'status' => 200,
        ]);
        
        $this->json('GET', '/tvshow/apiv1/')
             ->seeJson([
               'status' => 200,
        ]);        
        
        $this->json('GET', '/tvshowww/apiv1/')
             ->seeJson([
               'status' => 404,
        ]);  
        
        $this->json('GET', '/tvshow/apiv2/')
             ->seeJson([
               'status' => 404,
        ]);        
    }
}